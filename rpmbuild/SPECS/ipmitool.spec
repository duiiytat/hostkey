%define debug_package %{nil}

Name: ipmitool
Version: latest
Release: 1
Summary: IPMI management utilities
License: BSD
Group: Applications/System
URL: https://github.com/ipmitool/ipmitool

Source0: ipmitool-latest.tar.gz

BuildRequires: openssl-devel
BuildRequires: openldap-devel

%description
IPMItool provides a simple command-line interface to systems that support the Intelligent Platform Management Interface (IPMI) version 1.5 and version 2.0.

%prep
%setup -q

%build
./bootstrap
./configure
make

%install
make install
mkdir -p %{buildroot}/usr/bin/
install src/ipmitool %{buildroot}/usr/bin/ipmitool
install src/ipmievd %{buildroot}/usr/bin/ipmievd

%files
%defattr(-,root,root)
/usr/bin/ipmitool
/usr/bin/ipmievd

%changelog

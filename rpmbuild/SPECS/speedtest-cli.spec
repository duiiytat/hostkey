%define debug_package %{nil}

Name:           speedtest-cli
Version:        1.0.10
Release:        1
Summary:        This is a command line interface for LibreSpeed speed test backends, written in Go.

License:        GNU
URL:            https://github.com/librespeed/speedtest-cli/tree/v1.0.10
Source0:        v1.0.10.tar.gz

BuildRequires:  golang

%description
This is a command line interface for LibreSpeed speed test backends, written in Go.


%prep
%setup -q

%build
./build.sh 1.0.10
ls -lah out/

%install
mkdir -p %{buildroot}/usr/bin/
install out/librespeed-cli-linux-amd64 %{buildroot}/usr/bin/speedtest-cli


%files
%defattr(-,root,root)
/usr/bin/speedtest-cli

%changelog
 

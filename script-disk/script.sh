#!/bin/bash

# Получаем список дисков, отсортированных по размеру
disk_list=$(lsblk -rdbo KNAME,SIZE | grep -v "loop" | sort -k2 -n)

# Находим диск с наименьшим объемом, но не менее 20 гигабайт
disk=""
while IFS= read -r line; do
  size=$(echo "$line" | awk '{print $2}')
  if ((size >= 21474836480)); then
    disk="/dev/$(echo "$line" | awk '{print $1}')"
    break
  fi
done <<< "$disk_list"

if [[ -z $disk ]]; then
  echo "Не найдено подходящего диска."
  exit 1
fi

# Создаем раздел на первом диске размером 512MB
parted "$disk" -s --  mklabel msdos
parted -s "$disk" mkpart primary ext4 0% 512MB

# Создаем раздел на первом диске, используя все оставшееся место
parted -s "$disk" mkpart primary ext4 512MB 100%

# Форматируем первый раздел в ext4
mkfs.ext4 "${disk}1"

# Создаем LVM на втором разделе
pvcreate "${disk}2"
vgcreate vg_root "${disk}2"

# Создаем логический раздел для root и swap
lvcreate -L 4G -n lv_root vg_root
lvcreate -l 100%FREE -n lv_swap vg_root

# Форматируем разделы
mkfs.ext4 /dev/vg_root/lv_root
mkswap /dev/vg_root/lv_swap

echo "Диск с наименьшим объемом, но не менее 20 гигабайт: $disk"